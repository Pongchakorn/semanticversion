## [1.2.1](http://bitbucket.org/Pongchakorn/semanticversion/compare/v1.2.0...v1.2.1) (2020-06-17)


### Bug Fixes

* add new text ([bf814ee](http://bitbucket.org/Pongchakorn/semanticversion/commits/bf814ee4e265db942903816fd7d66a5a2bf33827))
* change text ([49705ce](http://bitbucket.org/Pongchakorn/semanticversion/commits/49705cef36a4934b0d748eded75c7ce33e2f4507))
* test merge branch ([362fad5](http://bitbucket.org/Pongchakorn/semanticversion/commits/362fad5a5a9979bd7eae4d924a50d22b285991d8))

# [1.2.0](http://bitbucket.org/Pongchakorn/semanticversion/compare/v1.1.0...v1.2.0) (2020-06-17)


### Bug Fixes

* test commit another branch ([6128aba](http://bitbucket.org/Pongchakorn/semanticversion/commits/6128ababbffc46b916c4cc297a76ebacea4a07cd))


### Features

* merge dev to master ([b5f529e](http://bitbucket.org/Pongchakorn/semanticversion/commits/b5f529ea77293bf00a41eedf4a306f6451aee209))

# [1.1.0](http://bitbucket.org/Pongchakorn/semanticversion/compare/v1.0.1...v1.1.0) (2020-06-17)


### Features

* import package.json in to app.tsx ([74522a9](http://bitbucket.org/Pongchakorn/semanticversion/commits/74522a9f96c5d72ea23b6da03274b2fbaac9299e))

## [1.0.1](http://bitbucket.org/Pongchakorn/semanticversion/compare/v1.0.0...v1.0.1) (2020-06-08)


### Bug Fixes

* test ([c2fab68](http://bitbucket.org/Pongchakorn/semanticversion/commits/c2fab686d61f45b40928838277bdacaf8fda732c))

# 1.0.0 (2020-06-08)


### Bug Fixes

* fasle ([7e5455d](http://bitbucket.org/Pongchakorn/semanticversion/commits/7e5455da990631cbc329001067c4abe3bf86367a))
* test ([15c9927](http://bitbucket.org/Pongchakorn/semanticversion/commits/15c992764f3a1f226445cf47661d0758557374f8))
* test ([b474c18](http://bitbucket.org/Pongchakorn/semanticversion/commits/b474c181e3f2639de33563f4d97ba81d5ebd1e87))
* test ([fa40fe5](http://bitbucket.org/Pongchakorn/semanticversion/commits/fa40fe53642c32b64896c9437e25d3203e887045))
